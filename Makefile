all: init

init:
	cd out && unzip ../data/simavr-master.zip && cd simavr-master && patch -p0 < ../../data/simavr-master.patch && make

clean:
	cd out && rm -r simavr-master