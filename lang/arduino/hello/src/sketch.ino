#include <avr/sleep.h>

void setup() {
	Serial.begin(9600);
	Serial.println("Hello world");
}

void loop() {
	delay(100);
	cli(); //disable interrupts
	sleep_cpu(); //simavr quits on sleep_cpu with disabled interrupts
}
