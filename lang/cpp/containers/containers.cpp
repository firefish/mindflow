#include <iostream>
#include <cstring>
#include <array>
#include <vector>
#include <deque>
#include <forward_list>
#include <list>

void print_title(std::string title) {
	std::cout << "### " << title << " ###" << std::endl;
}

void print_arr(int *arr, size_t len, std::string sep=" ") {
	for (size_t i = 0; i < len; i++) {
		std::cout << arr[i] << sep;
	}
	std::cout << std::endl;
}

void lang(std::string title) {
	int initialized[] = {1, 2, 3, 4, 5};
	int fixed[4];
	memset(fixed, 0, sizeof(fixed));
	int dynamic[initialized[2]];
	memset(dynamic, 0, sizeof(dynamic));

	print_title(title);
	print_arr(initialized, 4);
	print_arr(fixed, 4);
	print_arr(dynamic, 4);
}

template <class T>
void print_stl(T start, T end, std::string sep=" ") {
	for (auto it = start; it != end; ++it) {
		std::cout << *it << sep;
	}
	std::cout << std::endl;
}

void stl(std::string title) {
	std::array<int, 2> intarr = {};
	std::vector<int> intvec(3);
	std::deque<int> intdeq(4);
	std::forward_list<int> intflist(5);
	std::list<int> intlist(6);

	intarr[1] = 1;
	intvec[2] = 2;
	intdeq[3] = 3;
	*std::next(intflist.begin(), 4) = 4;
	*intlist.rbegin() = 5;

	print_title(title);
	print_stl(intarr.begin(), intarr.end());
	print_stl(intvec.begin(), intvec.end());
	print_stl(intdeq.begin(), intdeq.end());
	print_stl(intflist.begin(), intflist.end());
	print_stl(intlist.begin(), intlist.end());
}

int main() {
	lang("lang");
	stl("stl");
	return 0;
}