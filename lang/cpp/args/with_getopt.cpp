#include <iostream>
#include <cstdlib>
#include <getopt.h>

void usage() {
	std::cout << "Use: prog --cmd hello --name world --shift 0" << std::endl;
	exit(1);
}

int main(int argc, char **argv) {
	const char* const short_opts = "c:n:s:h";
	const option long_opts[] = {
		{"cmd", required_argument, nullptr, 'c'},
		{"name", required_argument, nullptr, 'n'},
		{"shift", required_argument, nullptr, 's'},
		{"help", no_argument, nullptr, 'h'},
		{nullptr, no_argument, nullptr, 0}
	};
	std::string cmd, name;
	int shift = 0;
	while (true) {
		const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

		if (-1 == opt) {
			break;
		}

		switch (opt) {
			case 'c':
				cmd = std::string(optarg);
				break;
			case 'n':
				name = std::string(optarg);
				break;
			case 's':
				shift = std::stoi(optarg);
				break;
			case 'h': // -h or --help
			case '?': // Unrecognized option
			default:
				usage();
				break;
		}
	}
	if (cmd == "hello") {
		for (int i = 0; i < shift; i++) {
			std::cout << ' ';
		}
		std::cout << "Hello " << name << std::endl;
	} else {
		usage();
	}
	return 0;
}