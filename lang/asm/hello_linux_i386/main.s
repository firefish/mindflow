global _start

section .text

_start:
  mov eax, 4        ; write
  mov ebx, 1
  mov ecx, msg
  mov edx, msglen
  int 0x80

  mov eax, 1        ; exit
  mov ebx, 0
  int 0x80

section .rodata
  msg: db "Hello world", 0xa
  msglen: equ $ - msg