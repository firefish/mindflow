#!/usr/bin/env python2
from collections import namedtuple, defaultdict
from array import array

def arrays():
	ba = array('b', [0])
	ba.append(1)
	try:
		ba.append(128)
	except OverflowError as e:
		assert str(e) == 'signed char is greater than maximum'
	print ba

def lists():
	lst = []
	lst.append(0)
	lst.extend([1]*3)
	lst[2] = 2
	lst[-1] = 3
	lst += [0, 4, 0, 5][1::2]
	assert list(range(6)) == lst
	print lst

	try:
		d = {}
		d[lst] = None
	except TypeError as e:
		assert str(e) == "unhashable type: 'list'"

def tuples():
	t = tuple([0, 1])
	d = {}
	d[t] = None
	nttype = namedtuple('Item', ['Title', 'Cost'])
	nt = nttype('Sandwich', 1)
	print t
	print nt

def sets():
	s0 = set()
	s1 = {0, 1}
	s2 = {x for x in [0, 1, 2]}
	print s0
	print s1
	print s2

def dicts():
	d0 = {0: 0}
	d1 = {0: 1, 'a': 'b'}
	d2 = {k: v for v, k in d1.iteritems()}
	d3 = {}
	dd = defaultdict(int)
	ddd = defaultdict(lambda: defaultdict(int))
	try:
		d3[0] += 1
	except KeyError as e:
		pass
	d3[0] = d3.get(0, 0) + 1
	dd[0] += 1
	ddd[0][0] += 1
	print d0
	print d1
	print sorted(d2.items(), key=lambda x: str(x[0]))
	print d3
	print dd
	print ddd.values()

def main():
	arrays()
	lists()
	tuples()
	sets()
	dicts()

main()
