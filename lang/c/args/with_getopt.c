#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

void usage() {
	printf("Use: prog --cmd hello --name world --shift 0\n");
	exit(1);
}

int main(int argc, char **argv) {
	const char* const short_opts = "c:n:s:h";
	const struct option long_opts[] = {
		{"cmd", required_argument, NULL, 'c'},
		{"name", required_argument, NULL, 'n'},
		{"shift", required_argument, NULL, 's'},
		{"help", no_argument, NULL, 'h'},
		{NULL, no_argument, NULL, 0}
	};
	char *cmd = NULL, *name = NULL;
	int shift = 0;
	while (1) {
		const int opt = getopt_long(argc, argv, short_opts, long_opts, NULL);

		if (-1 == opt) {
			break;
		}

		switch (opt) {
			case 'c':
				cmd = strdup(optarg);
				break;
			case 'n':
				name = strdup(optarg);
				break;
			case 's':
				shift = atoi(optarg);
				break;
			case 'h': // -h or --help
			case '?': // Unrecognized option
			default:
				usage();
				break;
		}
	}
	if (cmd && 0 == strcmp("hello", cmd)) {
		for (int i = 0; i < shift; i++) {
			printf(" ");
		}
		printf("Hello %s\n", name);
	} else {
		usage();
	}
	return 0;
}