#!/usr/bin/env python2
import argparse
from selenium import webdriver

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--path')
	args = parser.parse_args()

	options = webdriver.ChromeOptions()
	options.add_argument('headless')
	options.add_argument('disable-gpu')
	options.add_argument('no-sandbox')
	driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver', chrome_options=options)
	driver.get(args.path)
	print driver.find_element_by_tag_name('body').text.encode('utf8')

main()
